object Traits extends App {
  /**
   * Traits
   */
  trait DonutShoppingCartDao{
    def add(name: String): Long
    def update(name: String) : Boolean
    def search(name: String): String
    def delete(name: String): Boolean
  }

  class DonutShoppingCart extends DonutShoppingCartDao {
    override def add(name: String): Long = {
      println(name)
      1
    }

    override def update(name: String): Boolean = {
      println(name)
      true
    }

    override def search(name: String): String = {
      println(name)
      name
    }

    override def delete(name: String): Boolean = {
      println(name)
      true
    }
  }

  val donutShoppingCart: DonutShoppingCart = new DonutShoppingCart()
  donutShoppingCart.add("Vanilla")
  donutShoppingCart.update("Vanilla")

  val donutShoppingCart2: DonutShoppingCartDao = new DonutShoppingCart()
  donutShoppingCart2.add("glazed")
  donutShoppingCart2.add("glazed")

  /**
   * Trait with type parameter
   */
  trait DonutShoppingCartDao1[A] {

    def add(donut: A): Long

    def update(donut: A): Boolean

    def search(donut: A): A

    def delete(donut: A): Boolean

  }

  /**
   * Multiple traits
   */
  trait DonutInventoryService[A] {
    def checkStockQuantity(donut: A) : Int
  }

  class DonutShoppingCart3[A] extends DonutShoppingCartDao1[A] with DonutInventoryService[A]{
    override def add(donut: A): Long = 2

    override def update(donut: A): Boolean = true

    override def search(donut: A): A = donut

    override def delete(donut: A): Boolean = true

    override def checkStockQuantity(donut: A): Int = 10
  }

  val donutShoppingCart3: DonutShoppingCart3[String] = new DonutShoppingCart3[String]()
  println(donutShoppingCart3.add("qwerty"))

  /**
   * Dependency Injection Using Traits
   */
  trait DonutDatabase[A] {
    def addOrUpdate(donut: A) : Long
    def query(donut: A): A
    def delete(donut: A): Boolean
  }

  class CassandraDonutStore[A] extends DonutDatabase[A] {
    override def addOrUpdate(donut: A): Long = {
      println(s"CassandraDonutDatabase-> addOrUpdate method -> donut: $donut")
      1
    }

    override def query(donut: A): A = {
      println(s"CassandraDonutDatabase-> query method -> donut: $donut")
      donut
    }

    override def delete(donut: A): Boolean = {
      println(s"CassandraDonutDatabase-> delete method -> donut: $donut")
      true
    }
  }

  //we create a trait called DonutShoppingCartDao which will be the main API to perform CRUD operations
  // versus some storage layer. To this end, we define a val of type DonutDatabase[A] which will be injected
  trait DonutShoppingCartDao2[A] {

    val donutDatabase: DonutDatabase[A] // dependency injection

    def add(donut: A): Long = {
      println(s"DonutShoppingCartDao-> add method -> donut: $donut")
      donutDatabase.addOrUpdate(donut)
    }

    def update(donut: A): Boolean = {
      println(s"DonutShoppingCartDao-> update method -> donut: $donut")
      donutDatabase.addOrUpdate(donut)
      true
    }

    def search(donut: A): A = {
      println(s"DonutShoppingCartDao-> search method -> donut: $donut")
      donutDatabase.query(donut)
    }

    def delete(donut: A): Boolean = {
      println(s"DonutShoppingCartDao-> delete method -> donut: $donut")
      donutDatabase.delete(donut)
    }

  }

  trait DonutInventoryService1[A]{
    val donutDatabase: DonutDatabase[A] // dependency injection

    def checkStockQuantity(donut: A): Int = {
      println(s"DonutInventoryService-> checkStockQuantity method -> donut: $donut")
      donutDatabase.query(donut)
      1
    }
  }

  trait DonutShoppingCartServices[A] extends DonutShoppingCartDao2[A] with DonutInventoryService1[A]{
    override val donutDatabase: DonutDatabase[A] = new CassandraDonutStore[A]()
  }

  class DonutShoppingCart1[A] extends DonutShoppingCartServices[A]{

  }
  val donutShoppingCart1: DonutShoppingCart1[String] = new DonutShoppingCart1[String]()
  donutShoppingCart1.add("Vanilla Donut")
  donutShoppingCart1.update("Vanilla Donut")
  donutShoppingCart1.search("Vanilla Donut")
  donutShoppingCart1.delete("Vanilla Donut")
  println("\n")

  /**
   * Avoiding Cake Pattern
   */
  class DonutInventoryService3[T]{
    def checkStock(donut: T): Boolean = {
      println("checkstock")
      true
    }
  }
  class DonutPricingService[T] {
    def calculatePrice(donut: T): Double = {
      println("DonutPricingService->calculatePrice")
      2.50
    }
  }
  class DonutOrderService[T] {
    def createOrder(donut: T, quantity: Int, price: Double): Int = {
      println(s"Saving donut order to database: donut = $donut, quantity = $quantity, price = $price")
      100 // the id of the booked order
    }
  }
  class DonutShoppingCartService[T] (
                                      donutInventoryService3: DonutInventoryService3[T],
                                      donutPricingService: DonutPricingService[T],
                                      donutOrderService: DonutOrderService[T]) {

    def bookOrder(donut: T, quantity: Int): Int = {
      println("DonutShoppingCartService->bookOrder")

      if (donutInventoryService3.checkStock(donut)) {
        val price = donutPricingService.calculatePrice(donut)
        donutOrderService.createOrder(donut, quantity, price)
      } else {
        println(s"Sorry donut $donut is out of stock!")
        -100
      }
    }
  }
  trait DonutStoreServices {
    val donutInventoryService = new DonutInventoryService3[String]
    val donutPricingService = new DonutPricingService[String]
    val donutOrderService = new DonutOrderService[String]
    val donutShoppingCartService = new DonutShoppingCartService(donutInventoryService, donutPricingService, donutOrderService)
  }

  trait DonutStoreAppController {
    this: DonutStoreServices =>

    def bookOrder(donut: String, quantity: Int): Int = {
      println("DonutStoreAppController->bookOrder")
      donutShoppingCartService.bookOrder(donut, quantity)
    }
  }

  object DonutStoreApp extends DonutStoreAppController with DonutStoreServices
  DonutStoreApp.bookOrder("Vanilla Donut", 10)

  //You may be asking yourself why did we go through all the trouble of having a single facade?
  // One of the most obvious benefit is to make our DonutStoreApp easily testable.
  // All you need to do is to create a trait which will mock the DonutStoreServices.
  trait MockedDonutStoreServices extends DonutStoreServices {
    override val donutInventoryService: DonutInventoryService3[String] = ???
    override val donutPricingService: DonutPricingService[String] = ???
    override val donutOrderService: DonutOrderService[String] = ???
    override val donutShoppingCartService: DonutShoppingCartService[String] = new DonutShoppingCartService[String](
      donutInventoryService, donutPricingService, donutOrderService)
  }

  object MockedDonutStoreApp extends DonutStoreAppController with MockedDonutStoreServices

  /**
   * Traits, Companion Object, Factory Pattern
   */
  //Define a wrapper object called Cakes to hold various types of cakes
  object Cakes {

    // Define a base trait to represent a Cake
    trait Cake {
      def name: String
    }

    // Define class implementations for the Cake trait namely: Cupcake, Donut and UnknownCake
    class UnknownCake extends Cake {
      override def name: String = "Unknown Cake ... but still delicious!"
    }

    class Cupcake extends Cake {
      override def name: String = "Cupcake"
    }

    class Donut extends Cake {
      override def name: String = "Donut"
    }

  }

  //create another object named CakeFactory and
  // it will be a convenient wrapper for our factory pattern
  object CakeFactory{
    import Cakes._

    def apply(cake: String): Cake = { // Factory Method
      cake match {
        case "cupcake" => new Cupcake
        case "donut" => new Donut
        case _ => new UnknownCake
      }
    }
  }

  println(s"A cupcake = ${CakeFactory("cupcake").name}")
  println(s"A donut = ${CakeFactory("donut").name}")
  println(s"Unknown cake = ${CakeFactory("coconut tart").name}")

  /**
   * Type Class For Ad-hoc Polymorphism
   */
  // http://allaboutscala.com/tutorials/chapter-5-traits/type-class-for-adhoc-polymorphism/

  /**
   * Magnet Pattern
   */
  // http://allaboutscala.com/tutorials/chapter-5-traits/the-magnet-pattern/
}
