/**
 * http://allaboutscala.com/tutorials
 */

val x = 10
// x = 20 Immutable

var y = 10
y = 11 //Mutable

lazy val z = "qwerty" // Initialization delayed until required

print(z) // Now initialized

//Scala Supported types
val a_int: Int = 1
val a_long: Long = 1L
val a_short: Short = 1
val a_double: Double = 1.000
val a_float: Float = 1.0f
val a_string: String = "1"
val a_byte: Byte = 1
val a_char: Char = '1'
val a_unit: Unit = ()

//Declare variable with no initialization for future assignment
//var later: String = _
//later = "hello"
//
//later

/**
 * String Interpolation
 */
print("Hello World!!!")
val str = "qwerty"
println(s"Value of str : $str") // s prefixed for printing variables

case class Donut(name: String, tasteLevel: String)
val donut = Donut("qwerty", "tasty")
println(s"name : ${donut.name} and tasteLevel : ${donut.tasteLevel}")


println(s"${10==10}")

println(f"$str%20s")

println(a_double)
println(f"$a_double%.2f")

//val donutJson: String ="{"donut_name":"Glazed Donut","taste_level":"Very Tasty","price":2.50}"
//Error in declaring a JSON

//Using escape character
val donutJson2: String ="{\"donut_name\":\"Glazed Donut\",\"taste_level\":\"Very Tasty\",\"price\":2.50}"
println(s"donutJson2 = $donutJson2")
//or use triple quotes

/**
 * Type inference
 */

val x1 = 10 //inferred integer
//val x2: String = x1 Error
val x2 = x1.toString

val x3 = if(x1 > 10) 20 else 30

/**
 * Loops
 */
for(x <- 1 to 5) println(x)

for(x <- 1 until 5) println(x)

val donutIngredients = List("flour", "sugar", "egg yolks", "syrup", "flavouring")
for(x <- donutIngredients if x == "egg yolks") //Filter values using if conditions in for loop
  println(x)

val ingredients = for{
  ingredient <- donutIngredients
  if ingredient == "sugar" || ingredient == "syrup"
} yield ingredient
// Filter values using if conditions in for loop and return the result back using the yield keyword
print(ingredients)

//Using for comprehension to loop through 2-Dimensional array
val twoDArray = Array.ofDim[String](2,2)
twoDArray(0)(0) = "q"
twoDArray(0)(1) = "w"
twoDArray(1)(0) = "e"
twoDArray(1)(1) = "r"

for{x<-0 until 2
    y<-0 until 2} print(twoDArray(x)(y))

/**
 * Range
 */
val from1to5 = 1 to 5
print(from1to5)

val from1to4 = 1 until 5
print(from1to4)

val from0to10by2 = 0 to 10 by 2
print(from0to10by2)

print(from0to10by2.toList)
print(from1to4.toArray.mkString(" "))

/**
 * Pattern Matching
 * */
val donutType = "Glazed Donut"
donutType match{
  case "Glazed Donut" | "zxcv" => println("qwerty")
  case "Plain Donut" => println("asdf")
  case _ => println("Default")
}

donutType match{
  case donut if donut.contains("Glazed") => println("qwerty")
  case "Plain Donut" => println("asdf")
  case _ => println("Default")
}

val priceOfdDonut: Any = 2.5
priceOfdDonut match{
  case _: Int => print("Int")
  case _: Double => print("Double")
  case _: Any => print("Any")
}

/**
 * Tuples
 */
val glazedDonutTuple = Tuple3("Glazed Donut", "Very Tasty", 1.0)
println(glazedDonutTuple._1, glazedDonutTuple._2)

glazedDonutTuple.productIterator.foreach(println)
val strawberryDonut = Tuple3("Strawberry Donut", "Very Tasty", 2.50)
val plainDonut = Tuple3("Plain Donut", "Tasty", 2.0)

val donutList: List[(String, String, Double)] = List(glazedDonutTuple, strawberryDonut, plainDonut)

donutList.foreach{
  case("Plain Donut", taste, price) => println(taste, price)
  case d if d._1 == "Glazed Donut" => println(d._2)
  case _ => None
}

/**
 * Option and Some
 */

val glazedDonutTaste: Option[String] = Some("Very Tasty")
println(glazedDonutTaste.get)
//calling get() in a production system is generally a code smell as you may end up with the same NullPointerException

val glazedDonutName: Option[String] = None
//java.util.NoSuchElementException: None.get
//println(glazedDonutName.get)
println(glazedDonutName.getOrElse("None"))

glazedDonutName match{
  case Some(name) => println(name)
  case None       => print("None")
}

/**
 **********************************************************
 */

/**
 * Thinking In Terms of Functions
 */

/**
 * Function Basics
 */

def foo(): String = {
  "Qwerty"  //The last line within the body of the function is the one that will be returned back to the caller
}

foo()

def foo1 = "qwerty" //define and use a function with no parenthesis
foo1

def foo2(): Unit = { //define and use a function with no return type
  println("qwerty")
}

foo2()

def foo3(x: String, y: Int, z: String = "No Input"): Double = {
  println(x)
  println(z)
  2.0 * y
}

foo3("qwerty", 3)

// Best practice in place of giving default parameters
def foo4(x: String, y: Int, Coupon: Option[String] = None): Double = {
  println(x)
  Coupon match {
    case Some(_) =>
        val discount = 0.1
        val totalCost = 2.5 * y * (1 - discount)
        totalCost

    case None => 2.0 * y
  }
}

foo4("qwerty", 100, Some("FLAT40"))


/**
 * Implicit
 */
def totalCost(donutType: String, quantity: Int)(implicit discount: Double): Double = {
  println(s"Calculating the price for $quantity $donutType")
  val totalCost = 2.50 * quantity * (1 - discount)
  totalCost
}
//an implicit parameter is just a function parameter annotated with the implicit keyword.
// It means that if no value is supplied when called, the compiler will look for an implicit value and pass it in for you
implicit  val discount: Double = 0.1
totalCost("qwerty", 10)
totalCost("qwerty", 10)(0.3)

//class DonutString(s: String) {
//
//  def isFavoriteDonut: Boolean = s == "str1"
//
//}
//
////It is a good practice to encapsulate your implicit functions or conversions into a singleton using object
//object DonutConversions {
//  implicit def stringToDonutString(s: String): DonutString = new DonutString(s)
//}
//
//import DonutConversions._
////As part of the import expression, we are using the wildcard operator _ which will import any values or implicit functions
//
//val str1 = "str1"
//val str2 = "str2"
//
//println(str1.isFavoriteDonut)
//println(str2.isFavoriteDonut)

/**
 * Typed Functions
 */
//Implemented in class file
//def foo5(x: Double): Unit = {
//  print(x*2)
//}
//
//def foo5(x: Int): Unit = {
//  println(x)
//}
//
//foo5(2.0)
//foo5(2)

def foo5[T](x: T): Unit ={
  x match {
    case _: String => println(x)
    case _: Double => println(x)
    case _ => println("Nothing")
  }
}

foo5[String]("qwerty")
foo5[Double](2.0)

def foo6[T](x: T): Seq[T]={
  x match{
    case _: String =>
      println("String Type")
      Seq[T](x)
    case _: Double=>
      println("Double Type")
      Seq[T](x)
    case _ => println("Unsupported type")
      Seq[T](x)
  }
}

foo6[String]("qwerty")
foo6[Double](1.0)
foo6[Char]('A')

/**
 * Variable arguments
 */
def foo7(x: String*): Unit ={
  print(x.mkString(" , "))
}

foo7("abc", "qwerty", "asdf")
//foo7(List("abc", "qwerty", "asdf"))

foo7(List("abc", "qwerty", "asdf"): _*) // Type ascription
//you can use Scala’s _* operator to adapt a sequence (Array, List, Seq, Vector, etc.) so it can be used as an argument for a varargs field

class DonutCostCalculator {
  val totCost = 100

  def minusDiscount(discount: Double): Double = {
    totCost - discount
  }

  def -(discount: Double): Double = {
    totCost - discount
  }
}

val donutCost = new DonutCostCalculator()
donutCost.minusDiscount(20)
donutCost.-(99.5)
println(donutCost - 30)

/**
 * Function Currying
 */
//create functions whose parameters are organized into parameter groups - also known as Function Currying.
def foo8(x: Int)(y: String)(z: Double): Unit ={
  println(x,y,z)
}

foo8(1)("qwerty")(2.0)

val foo9 = foo8(1) _ // Return type String=>Double=>Unit
foo9("qwerty")(2.0)

def foo10(x: Int)(y: String)(f: Double=>Double): Double ={
  println(x,y)
  val w = 3.5
  f(w)
}

val xyz = foo10(1)("qwerty"){ z =>
  z * 100
}

xyz

def xyz1(z: Double): Double = {
  z*100
}

foo10(1)("qwerty")(xyz1)

/**
 * Call By Name Function
 */
def foo11(x: Int)(y: Double): Double = { // A simple function
  x*y
}

foo11(2)(2.0)

import scala.util.Random
val randomNumber = new Random(10)
def randomNumber_generate: Double = {
  val z = randomNumber.nextDouble()
  println(z)
  z
}

def foo12(x: Int)(y: => Double): Double = { // Call by name function used in y
  x*y
}

foo12(2)(randomNumber_generate)
foo12(2)(randomNumber_generate)
foo12(2)(randomNumber_generate)
// For each call to foo12, a new value for y is being calculated as randomnumber_generator is called

/**
 * Function Call Back
 */
def printReport(sendEmailCallback: () => Unit) {
  println("Printing report ... started")
  println("Printing report ... finished")
  sendEmailCallback()
}

printReport(()=>
println("Job Done"))

//printReport()
//Compile time error

printReport(()=>{}) // Doen't look elegant, there is a better way using option

def printReportWithOptionCallback(sendEmailCallback: Option[() => Unit] = None) {
  println("Printing report ... started")
  println("Printing report ... finished")
  sendEmailCallback.map(callback => callback())
}
//using the map() function to filter out any None callback function

printReportWithOptionCallback() // better way

printReportWithOptionCallback(Some(()=>
println("Job Done....from Some")))

/**
 * Create Function Using The Val Keyword Instead Of Def
 * Function Composition Using AndThen
 */

//Val functions inherit the andThen function
val x: Double = 10
val foo13 = (amount: Double) => {
  println("foo13 called")
  amount * 20
}

foo13(2.0)

val foo14 = (amount: Double) => {
  println("foo14 called")
  amount * 100
}

(foo13 andThen foo14)(2.0)
//Calling andThen will take the result from the first function
// and pass it as input parameter to the second function

/**
 * Function Composition Using Compose
 */
//Val functions inherit the compose function

//Mathematically speaking, (f compose g)(x) = f(g(x)).
//The second function g(x) is ran first and its result is passed along to the function f(x)
(foo13 compose(foo14))(2.0)
//Ordering using andThen: f(x) andThen g(x) = g(f(x))
//Ordering using compose: f(x) compose g(x) = f(g(x))

/**
 * Tail Recursion
 */
//Tail recursive function will help prevent overflow in your call stack because the evaluation of your looping construct happens at each step.
val arrayDonuts: Array[String] = Array("Vanilla Donut", "Strawberry Donut", "Plain Donut", "Glazed Donut")

@annotation.tailrec
def search(name: String, donuts: Array[String], index:Int): Option[Boolean] = {
  if(donuts.length == index){
    None
  } else if(donuts(index) == name){
    Some(true)
  } else {
    search(name, donuts, index+1)
  }
}

search("Glazed Donut", arrayDonuts, 0)
search("qwerty", arrayDonuts, 0)

import scala.util.control.TailCalls._
def tailSearch(name: String, donuts: Array[String], index:Int): TailRec[Option[Boolean]] = {
  if(donuts.length == index){
    done(None)
  } else if(donuts(index) == name){
    done(Some(true))
  } else {
    tailcall(tailSearch(name, donuts, index+1))
  }
}

val tailFound = tailcall(tailSearch("Glazed Donut", arrayDonuts, 0))
tailFound.result

/**
 * Trampoline Tail Recursive Function
 */
//Implemented in class file
//Say you had two tail recursive functions F(A) and F(B) and that F(A) calls F(B) but in turn F(B) also calls F(A).
//Then F(A) is said to be a trampoline tail recursive function because the call stack jumps back and forth between the two functions F(A) and F(B) - hence the name trampoline.
//def verySweetDonut(donutList: List[String]): TailRec[Boolean] = {
//  println(s"verySweetDonut function: donut list = $donutList")
//  if (donutList.isEmpty) {
//    println("verySweetDonut function: donut list isEmpty, returning false")
//    done(false)
//  } else {
//    if(Set(donutList.head).subsetOf(Set("Vanilla Donut","Strawberry Donut","Glazed Donut"))) {
//      println(s"verySweetDonut function: found donut list's head = ${donutList.head} to be VERY sweet, returning true")
//      done(true)
//    } else {
//      println(s"verySweetDonut function: donut list's head = ${donutList.head} is NOT VERY sweet, forwarding donut list's to notSweetDonut function")
//      tailcall(notSweetDonut(donutList))
//    }
//  }
//}
//
//def notSweetDonut(donutList: List[String]): TailRec[Boolean] = {
//  println(s"notSweetDonut function: with donut list = $donutList")
//  if (donutList.isEmpty) {
//    println("notSweetDonut function: donut list isEmpty, returning false")
//    done(false)
//  } else {
//    println(s"notSweetDonut function: donut list's head = ${donutList.head} is NOT sweet,   forwarding donut list's tail to verySweetDonut function")
//    tailcall(verySweetDonut(donutList.tail))
//  }
//}
//
//val donutList1: List[String] = List("Plain Donut", "Strawberry Donut", "Plain Donut", "Glazed Donut")
//val foundVerySweetDonut = tailcall(verySweetDonut(donutList1)).result
//println(s"Found very sweet donut = $foundVerySweetDonut")

/**
 * Create Partial Function Using the PartialFunction Trait
 */
val isVeryTasty: PartialFunction[String, String] = {
  case "Glazed Donut" | "Strawberry Donut" => "Very Tasty"
}

val isTasty: PartialFunction[String, String] = {
  case "Plain Donut" => "Tasty"
}

val unknownTaste: PartialFunction[String, String] = {
  case donut @ _ => s"Unknown taste for donut = $donut"
}

val donutTaste = isVeryTasty orElse isTasty orElse unknownTaste
println(donutTaste("Glazed Donut"))
println(donutTaste("Plain Donut"))
println(donutTaste("Chocolate Donut"))

/**
 * Nested Functions
 */
//let's assume that the validation logic is tightly coupled with the function itself
// and should not be visible by any other functions. As a result, we can use a Nested Function
def foo15(name: String): Boolean = {

  val validate = (name: String) => {
    name.nonEmpty && name.length > 0
  }

  validate(name)
}

foo15("qwerty")

//Continued in ClassesAndObjetcs.scala