object ClassesAndObjects extends App {
  /**
   * Classes and objects
   */
//  class Donut(name: String, productCode: Option[Long] = None){
//    val print: Unit = println(name, productCode.getOrElse(0))
//  }
//
//  val glazedDonut = new Donut("qwerty",Some(1234))
//  glazedDonut.print

  /**
   * Companion Objects
   */
//  A Companion Object is defined using the object keyword
//   and the name of the object should be identical to the class name.
//  In addition, the companion object should define an apply() method
//   which will be responsible for instantiating an instance of the given class

  //  object Donut{
//    def apply(name: String, productCode: Long): Donut = new Donut(name, Some(productCode))
//  }
//
//  val donut1 = Donut("qwerty", 12345)
//  donut1.print

  /**How To Use Companion Objects' Apply Method As A Factory*/
//  class GlazedDonut(name: String) extends Donut(name)
//  class VanillaDonut(name: String) extends Donut(name)
//
//  object Donut{
//    def apply(name: String): Donut = {
//      name match{
//        case "Glazed Donut" => new GlazedDonut(name)
//        case "Vanilla Donut" => new VanillaDonut(name)
//        case _ => new Donut(name)
//      }
//    }
//  }
//  val glazedDonut1 = Donut("Glazed Donut")
//  val vanillaDonut1 = Donut("Vanilla Donut")
//  glazedDonut.print
//  vanillaDonut1.print

  /**How To Declare Values And Fields In Companion Object*/
//  class Donut(name: String, productCode: Option[Long] = None){
//
//    val print: Unit = println(s"Donut name = $name, productCode = ${productCode.getOrElse(0)}, uuid = ${Donut.uuid}")
//
//  }
//
//  //Donut Companion Object, we've defined a value named uuid but also marked it as private
//  //Companion Object works along side the class to which the Companion Object refers to, even though the uuid field is not visible to the outside world,
//  // it is still accessible within the Donut class
//  object Donut {
//    private val uuid = 1
//
//    def apply(name: String, productCode: Option[Long]): Donut = {
//      new Donut(name, productCode)
//    }
//
//    def apply(name: String): Donut = {
//      new Donut(name)
//    }
//  }
//
//  val glazedDonut = Donut("qwerty", Some(1111))
//  val qwerty = Donut("asdf")
//  glazedDonut.print
//  qwerty.print

  /**
   * Singleton Object
   */
  // the Singleton Pattern is a a fairly common design pattern when you need exactly one instance of an object
  //The Singleton Pattern is so commonly used that Scala has made it very easy to create single instances of an object.
  // All you need to do is to make use of the keyword object
//  object DonutShoppingCalculator{
//    //Scala does not have a static keyword! Instead, you can simply encapsulate a global field using the val keyword inside a Singleton Object
//    val discount = 0.01
//
//    // Scala does not exposed a static keyword. But you can encapsulate functions and
//    // methods inside a Singleton Object if you would like to expose some globally accessible utility function or method.
//    def calculateTotalCost(donuts: List[String]): Double = {
//      1
//    }
//  }
//
//  println(DonutShoppingCalculator.discount)
//  println(DonutShoppingCalculator.calculateTotalCost(List("qwerty")))

  /**
   * Case Class
   */
//  // A case class is similar to any other classes except that it also creates the Companion Object.
//  // In addition, a case class will automatically create the apply(),  toString(), hashCode and equals() methods for you
//  case class Donut(name: String, price: Double, productCode: Option[Long] = None)
//  val vanillaDonut: Donut = Donut("Vanilla Donut", 1.50)
//  val glazedDonut: Donut = Donut("Glazed Donut", 2.0)
//  println(vanillaDonut)
//  println(vanillaDonut.name)
//  println(glazedDonut)
//  //Scala favours the use of immutability. As a results, fields defined on case class are immutable by default
//  // vanillaDonut.name = "qwerty" // Error
//
//  //In case class, Scala compiler automatically creates the hashCode and equals() methods for you
//  //A case class also comes with a handy copy() method which you can use to copy an object and override any field.
//  // In return, the copy() method will create new instances of the given object

  /**
   * Type Aliasing
   */
//  type abhi = Int
//  val x: abhi = 1
//  print(x)

  /**
   * Implicit Class
   */
//  //we will learn how use Implicit Class to add methods to an object without modifying the source code of the object - also commonly known as extension methods
//  //Using Implicit Class to extend functionality of an object can be quite handy especially when you do have have access to modify the source object.
//  case class Donut(name: String, price: Double, productCode: Option[Long] = None)
//  val vanillaDonut: Donut = Donut("Vanilla", 1.50)
//
//  //Assume that you were given a Donut type in a library or dependency and as such do not have access to modify the Donut source code.
//  // In addition, say you are given a requirement to create a unique identifier for the Donut type.
//  //With Implicit Class, you can easily extend the functionality of the Donut type. In the example below,
//  // we add a new method named uuid which returns a String and it uses the name and productCode of the Donut type to construct the unique id.
//  object DonutImplicits{
//    implicit class AugmentedDonut(donut: Donut){
//      def uuid: String = donut.name +  "-" + donut.productCode.getOrElse("1234")
//    }
//  }
//
//  //Since we've wrapped and encapsulated the AugmentedDonut Implicit Class inside the DonutImplicits object, to use the Implicit AugmentedDonut class you simply need to import it
//  import DonutImplicits._
//  println(vanillaDonut.uuid)
//  //The vanilla donut object now has access the uuid method - also known as the extension method.
  /**
   * Inheritance
   */
//  abstract class Donut(name: String) {
//    def printName(): Unit
//  }
//
//  class VanillaDonut(name: String) extends Donut(name){
//    override def printName(): Unit = println(name)
//  }
//
//  object VanillaDonut{
//    def apply(name: String): VanillaDonut = new VanillaDonut(name)
//  }
//
//  val vanillaDonut: Donut = VanillaDonut("vanilla")
//  vanillaDonut.printName()
//
//  //Case class extends
//  case class GlazedDonut(name: String) extends Donut(name){
//    override def printName(): Unit = println(name)
//  }
//
//  val glazedDonut: Donut = GlazedDonut("glazed")
//  glazedDonut.printName()
//
//  class ShoppingCart[D <: Donut](donuts: Seq[D]){
//    def printCartItems(): Unit = donuts.foreach(_.printName())
//  }
//  //With the notation [D <: Donut], we are restricting only Donut types to be passed-through to the ShoppingCart class
//
////  You will get compiler error if you try to instantiate a ShoppingCart class with a different type other than the Donut type.
//  val shoppingCart: ShoppingCart[Donut] = new ShoppingCart[Donut](Seq[Donut](vanillaDonut, glazedDonut))
//  shoppingCart.printCartItems()
//
//  //Enabling Covariance
//  class ShoppingCart2[+D <: Donut](donuts: Seq[D]){
//    def printCartItems(): Unit = donuts.foreach(_.printName())
//  }
//  //We've enabled covariance of type Donuts using the notation [+D <: Donut]
//  //In other words, you can now create instances of ShoppingCart of type Donut or sub-types of Donuts such as VanillaDonut
//
//  val vanillaDonut1: VanillaDonut = VanillaDonut("vanilla")
//  vanillaDonut.printName()
//  val shoppingCart2: ShoppingCart2[Donut] = new ShoppingCart2[VanillaDonut](Seq(vanillaDonut1))
//  shoppingCart2.printCartItems()
//
//  //Enable Contra-Variance
//  class ShoppingCart3[-D <: Donut](donuts: Seq[D]){
//    def printCartItems(): Unit = donuts.foreach(_.printName())
//  }
//  //We've enabled contra-variance of type Donuts using the notation [-D <: Donut]
//  //In other words, you can now have a ShoppingCart of type VanillaDonut ShoppingCart[VanillaDonut] reference ShoppingCart of type Donut ShoppingCart2[Donut]
//  val shoppingCart3: ShoppingCart3[VanillaDonut] = new ShoppingCart3[Donut](Seq(glazedDonut))
//  shoppingCart3.printCartItems()

//  //Continued in Traits.scala
}

