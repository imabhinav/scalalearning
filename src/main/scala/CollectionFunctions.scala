object CollectionFunctions extends App {
  /**
   * Aggregate
   * The aggregate method aggregates results by first applying a sequence operation which is its first parameter and then uses a combine operator to combine the results produced by the sequence operation
   */
  val setString : Set[String] = Set("qwerty", "asdf")
  println(setString)

  // Value Function
  val setStringAccumulator: (Int, String) => Int = (accumulator, str) => accumulator + str.length

  val totalLength = setString.aggregate(0)(setStringAccumulator, _+_)
  println(totalLength)
  println()

  /**
   * Collect
   * The collect method takes a Partial Function as its parameter and applies it to all the elements in the collection to create a new collection which satisfies the Partial Function
   */
  val listAny = List("qwerty", 12234, "asdf", 99)
  println(listAny)

  //cherry pick string
  val stringsFromListAny = listAny.collect{case name: String => name}
  println(stringsFromListAny)

  val intFromListAny = listAny.collect{case name: Int => name}
  println(intFromListAny)
  println()

  /**
   * diff
   * The diff method takes another Set as its parameter and uses it to find the elements that are different from the current Set compared to another Set.
   */
  val setString1 = Set("qwerty", "zxcv", "wasd", "qaz")
  val diffSet: Set[String] = setString diff setString1
  println(diffSet)

  val diffSet1 = setString -- setString1 // Using --
  println(diffSet1)
  println()

  /**
   * drop
   * The drop method takes an integer parameter N and will return a new collection that does not contain the first N elements
   */
  println(setString1.drop(2)) // Store into a new val
  println(setString1)
  println()

  /**
   * dropWhile
   * The dropWhile method takes a predicate function parameter that will be used to drop certain elements in a collection which satisfies the predicate function.
   * The "dropping" process, or removal of elements, will stop as soon as it encounters an element that does not match the predicate function.
   * Therefore, it will return a new collection with the remaining elements from the original collection, starting from the first element that did not match the predicate function
   */
  println(setString1.dropWhile(name => name.charAt(0) == 'q'))
  println()

  /**
   * exists
   * The exists method takes a predicate function and will use it to find the first element in the collection which matches the predicate
   */
  println(setString1.exists(name => name.equals("qwerty")))
  println()

  /**
   * filter
   * The filter method takes a predicate function as its parameter and uses it to select all the elements in the collection which matches the predicate.
   * It will return a new collection with elements that matched the predicate.
   */
  println(setString1.filter(name => name.contains("rty") || name.contains("sd")))
  println(setString1.filterNot(name => name.contains("rty") || name.contains("sd")))

  /**
   * find
   * The find method takes a predicate function as parameter and uses it to find the first element in the collection which matches the predicate. It returns an Option and
   * as such it may return a None for the case where it does not match any elements in the collection with the predicate function.
   */
  println(setString1.find(name => name=="qwerty"))
  println(setString1.find(_ == "qwerty"))
  println(setString1.find(_ == "mnbvc"))
  println(setString1.find(_ == "mnbvc").getOrElse("Not Found"))
  println()

  /**
   * flatMap
   * The flatMap method takes a predicate function, applies it to every element in the collection. It then returns a new collection by using the elements returned by the predicate function.
   * The flatMap method is essentially a combination of the map method being run first followed by the flatten method.
   */
  val listOfSetOfString = List(setString1, setString)
  println(listOfSetOfString)
  println(listOfSetOfString.flatMap(seq => seq))
  println()

  /**
   * flatten
   * The flatten method will collapse the elements of a collection to create a single collection with elements of the same type.
   */
  println(listOfSetOfString.flatten)
  println(listOfSetOfString.flatten.map(_ + " suffix"))
  println(listOfSetOfString.flatMap(_ + " suffix"))
  println()

  /**
   * fold
   * The fold method takes an associative binary operator function as parameter and will use it to collapse elements from the collection.
   * The fold method allows you to also specify an initial value
   */
  val seqOfInt = List(1,5,4,3,2)
  println(seqOfInt.fold(0)(_ + _))
  println(setString1.fold("prefix ")((a, b) => a + b + " suffix "))

  /**
   * foldLeft
   * The foldLeft method takes an associative binary operator function as parameter and will use it to collapse elements from the collection.
   * The order for traversing the elements in the collection is from left to right and hence the name foldLeft.
   * The foldLeft method allows you to also specify an initial value
   *  foldLeft is fundamental in recursive function and will help you prevent stack-overflow exceptions
   */
  println(seqOfInt.foldLeft(0)(_ + _))
  println(setString1.foldLeft("prefix ")((a, b) => a + b + " suffix "))
  println()

  /**
   * foldRight
   * The foldRight method takes an associative binary operator function as parameter and will use it to collapse elements from the collection.
   * The order for traversing the elements in the collection is from right to left and hence the name foldRight.
   * The foldRight method allows you to also specify an initial value.
   * Prefer using foldLeft as opposed to foldRight since foldLeft is fundamental in recursive function and will help you prevent stack-overflow exceptions.
   */
  println(setString1.foldRight("prefix ")((a, b) => a + " " + b + " suffix "))
  println()

  /**
   * foreach
   * The foreach method takes a function as parameter and applies it to every element in the collection.
   * As an example, you can use foreach method to loop through all elements in a collection.
   */
  setString1.foreach(println(_))
  setString1.foreach(a => println(s"String = ${a.toUpperCase()}"))
  println()

  /**
   * groupBy
   * The groupBy method takes a predicate function as its parameter and uses it to group elements by key and values into a Map collection
   */
  println(setString1.groupBy(_.charAt(0)))
  println()

  /**
   * head
   * The head method will return the first element in the collection.
   */
  println(setString1.head)
  println(setString1.headOption.getOrElse("Nothing Found")) // In case empty
  println()

  /**
   * isEmpty
   * The isEmpty method will check whether a given collection is empty and will return either true or false
   */
  println(setString1.isEmpty)
  println()

  /**
   * intersect
   * The intersect method will find the common elements between two Sets
   */
  println(setString1 intersect setString)
  println(setString1 & setString)
  println()

  /**
   * map
   * The map method takes a predicate function and applies it to every element in the collection.
   * It creates a new collection with the result of the predicate function applied to each and every element of the collection
   */
  println(setString1.map(_ + " suffix"))
  println()

  //println(seqOfInt.map{a => seqOfInt.take(seqOfInt.indexOf(a)) :+ a}.collect(_.sum).last)
  //println(x.sum)

  /**
   * maxBy/ max/ min/ minBy
   * max -> max value
   * min -> min value
   * minBy -> min value of applied function
   * The maxBy method takes a predicate function as its parameter and applies it to every element in the collection to return the largest element.
   */
  println(setString1.maxBy(a => a.length))
  println()
  println(setString1.max) //the max method is using the natural order of String
  println()
  println(setString1.min)
  println()
  println(setString1.minBy(a => a.length))
  println()

  /**
   * mkString
   * The mkString method will help you create a String representation of collection elements by iterating through the collection.
   * The mkString method has an overloaded method which allows you to provide a delimiter to separate each element in the collection.
   * Furthermore, there is another overloaded method to also specify any prefix and postfix literal to be prepended or appended to the String representation..
   */
  println(setString1.mkString(" and "))
  println(setString1.mkString("Prefix ", " and ", " Suffix"))
  println()

  /**
   * partition
   * The partition method takes a predicate function as its parameter and will use it to return two collections:
   * one collection with elements that satisfied the predicate function and another collection with elements that did not match the predicate function.
   */
  println(setString1.partition(a => a.charAt(0) == 'q'))
  println()

  /**
   * reduce
   * The reduce method takes an associative binary operator function as parameter and will use it to collapse elements from the collection.
   * Unlike the fold method, reduce does not allow you to also specify an initial value
   */
  println(seqOfInt.reduce(_ + _))
  println(setString1.reduce((a,b) => a + " , " + b))
  println(setString1.reduceLeft((a,b) => a + " , " + b + " suffix "))
  println(setString1.reduceRight((a,b) => a + " , " + b + " suffix "))
  println(Set.empty[String].reduceOption(_ + " , " + _))
  println()

  /**
   * sorted
   * Will sort the list using the natural ordering (based on the implicit Ordering passed)
   *
   * sortBy (an attribute)
   * Sort by a given attribute using the attribute's type.
   * e.g. given a list of Person objects, if you want to sort them in ascending order of their age (which is an Int), you could simply say: personList.sortBy(_.age)
   *
   * sortWith (a function)
   * Takes a comparator function. Useful when you want to specify a custom sorting logic.
   * e.g. if you want to sort by age descending, you could write this as:
   *
   * personList.sortWith{(leftE,rightE) =>
   *      leftE.age > rightE.age
   * }
   *
   * Or, more simply: personList.sortWith(_.age > _.age)
   */

  /**
   * tail : The tail method returns a collection consisting of all elements except the first one
   * take : The take method takes an integer N as parameter and will use it to return a new collection consisting of the first N elements
   * takeRight : The takeRight method takes an integer N as parameter and will use it to return a new collection consisting of the last N elements
   * takeWhile : The takeWhile method takes a predicate function and will use it to return a new collection consisting of elements which match the predicate function.
   * transpose : The transpose method will pair and overlay elements from another collections into a single collection.
   * union : The union method takes a Set as parameter and will merge its elements with the elements from the current Set.
   * unzip : The unzip method will unzip and un-merge a collection consisting of element pairs or Tuple2 into two separate collections
   * view : The view method will create a non-strict version of the collection which means that the elements of the collection will only be made available at access time
   * withFilter : The withFilter method takes a predicate function and will restrict the elements to match the predicate function. withFilter does not create a new collection while filter() method will create a new collection
   * zipWithIndex : The zipWithIndex method will create a new collection of pairs or Tuple2 elements consisting of the element and its corresponding index
   */
  println(List(setString1.take(3), seqOfInt.take(3)).transpose)
  println(setString1.take(3) zip seqOfInt)
  println((setString1.take(3) zip seqOfInt).unzip)
  //filter all the even numbers and finally only take the first 10 odd numbers
  //Running computation on the large list would require to eagerly build the elements of the list which could result in the typical OutOfMemory exception
  println((1 to 1000000).filter(_ % 2 != 0).take(10).toList)
  //How to lazily create a large numeric range and take the first 10 odd numbers
  println((1 to 1000000).view.filter(_ % 2 != 0).take(10).toList)
  setString1.withFilter(_.charAt(0) == 'q').foreach(println(_))
  println(setString1.zipWithIndex)
  println()
}
