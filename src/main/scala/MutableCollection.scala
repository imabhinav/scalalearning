import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

object MutableCollection extends App{
  /**
   * Array
   */
  //An Array is a mutable data structure of fixed length. It also allows you to access and modify elements at specific index
  val array1: Array[String] = Array("a", "b", "c")
  println(array1.mkString(" , "))
  println(array1(2))

  val array2: Array[String] = new Array(3)
  array2(0) = "a"
  array2(2) = "c"
  println(array2.mkString(" "))

  val array3: Array[Array[String]] = Array.ofDim[String](2,2)
  array3(0)(0) = "a"
  array3(0)(1) = "b"
  array3(1)(0) = "c"
  array3(1)(1) = "d"
  println(array3.foreach(x => println(x.mkString(" "))))

  val array4: Array[Int] = Array.tabulate(5)(_ + 1)
  println(array4.mkString(" "))

  val rangeArray: Array[Int] = (1 to 10).toArray[Int]
  println(rangeArray.mkString(" "))

  val copyOfRangeArray: Array[Int] = new Array[Int](rangeArray.length)
  Array.copy(rangeArray, 0, copyOfRangeArray, 0, rangeArray.length)
  println(copyOfRangeArray.mkString(" "))

  val clonedArray = rangeArray.clone()
  println(clonedArray.mkString(" "))

  val array5 = Array("a", "b", "c")
  println(array1==array5) // Prints false
  println(array1 sameElements array5)

  /**
   * ArrayBuffer
   */
  // ArrayBuffer is resizable while an Array is fixed in size
  val arrayBuffer1 = ArrayBuffer("a", "b", "c")
  println(arrayBuffer1)

  arrayBuffer1 += "d" // add element
  println(arrayBuffer1)

  arrayBuffer1 ++= List[String]("e", "f") // add list of elements
  println(arrayBuffer1)

  arrayBuffer1 -= "d" // remove element
  println(arrayBuffer1)

  arrayBuffer1 --= List[String]("b")
  println(arrayBuffer1)
  println()

  /**
   * ArrayStack, now Depreciated use Stack instead
   */
  //val arrayStack: mutable.ArrayStack["String"] = mutable.ArrayStack("a", "b")
  val stack1: mutable.Stack[String] = mutable.Stack("a", "b", "c")
  //Similar operations as Array for addition of elements
  println(stack1)
  println(stack1.pop())
  println(stack1)

  stack1.push("d")
  println(stack1)
  println()

  /**
   * ListBuffer
   */
  //As per the Scala Documentation, a ListBuffer is resizable similar to an ArrayBuffer, except that it uses a Linked List as its internal data structure.
  val listBuffer1: ListBuffer[String] = ListBuffer("a", "b", "c")
  println(listBuffer1)
  // Same operation as ArrayBuffer

  /**
   * Map
   */
  val map1: mutable.Map[String, String] = mutable.Map("a"->"A", "b"->"B")
  println(map1)

  map1 += ("c"->"C")
  // ++= to add map
  // -= to remove element
  println(map1)
  println()

  /**
   * HashMap
   * As per the Scala Documentation, a HashMap is a collection of key and value pairs which are stored internally using a Hash Table data structure.
   * Same operations as Map
   */

  /**
   * ListMap
   * As per the Scala Documentation, a ListMap is a collection of key and value pairs where the keys are backed by a List data structure.
   * Same Operations as Map
   */

  /**
   * LinkedHashMap
   * As per the Scala Documentation, a LinkedHashMap is a collection of key and value pairs which are stored internally using Hash Table data structure. But iterating through the elements is done in order.
   */

  /**
   * Queue
   * Same operations as immutable queue
   */

  /**
   * Priority Queue
   * As per the Scala Documentation, a mutable PriorityQueue is implemented using a heap data structure and only the method dequeue and dequeueAll will return the elements in their priority order
   */
  case class Donut(name: String, price: Double)
//  object donutOrder extends Ordering[Donut]{
//    override def compare(x: Donut, y: Donut): Int = (x.price).compareTo(y.price)
//  }

  //Alternate Way
  def donutOrder(d: Donut) = d.price // -d.price for reverse ordering

  val priorityQueue1: mutable.PriorityQueue[Donut] = mutable.PriorityQueue(
    Donut("a", 1.5),
    Donut("b", 2.5),
    Donut("c", 2.0)
  )(Ordering.by(donutOrder))
  println(priorityQueue1)

  priorityQueue1.enqueue(Donut("d",0.5))
  println(priorityQueue1)

  println(priorityQueue1.dequeue())
  println(priorityQueue1.dequeue())

  /**
   * Set/HashSet/SortedSet/TreeSet/LinkedHashSet/BitSet, Same operations as immutable
   */

}
